# FEA Tools

Julia functions to aid pre- and post- processing of FEA studies using ElmerFEM and ParaView.